highscores
==========

A standalone binary to return user highscores from pleroma database, specifically the activity table.

Highscore is calculated using number of reports and blocks each user received with formula

```
highscore = reports * 10 + blocks
```

Only users that set their profiles as discoverable in privacy settings are returned by this api.

Only top 100 users are returned currently.

Only reports that are closed without being resolved are counted.

Results can be cached with configurable timeout, 1 second by default.
Higher cache periods make sense from data update intervals (~1 minute would be ideal) but can lead to negative user experience when updating privacy settings.
This can be workarounded by providng means to reset the cache, but that will require either database-side triggers or changes to pleroma itself to implement.

Keep in mind that not all blocks and reports are federated. 

Some contrib files (systemd unit/sample static html to consume the api/nginx configs) are available.

Building
--------

```sh
go build ./cmd/pleroma-highscores
```

go 1.19+ is required. Lower versions can be supported by editing go.mod.

Running
-------

```sh
./pleroma-highscores -h
```
