module gitlab.eientei.org/eientei/highscores

go 1.19

require (
	github.com/jmoiron/sqlx v1.3.5
	github.com/lib/pq v1.10.7
	github.com/patrickmn/go-cache v2.1.0+incompatible
)
