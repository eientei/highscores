package main

import (
	"context"
	"encoding/json"
	"flag"
	"net/http"
	"strconv"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	gocache "github.com/patrickmn/go-cache"
)

const queryHighscores = `
with
sepcounts as (
  select
    recipients[1] as account,
    data->>'type' as type,
    count(*) as count
  from activities where
    (data->>'type' = 'Flag' and data->>'state' != 'resolved') and
    recipients[1] like ('https://' || $1 ||'/%')
  group by recipients[1], data->>'type'
  union
  select
    users.ap_id as account,
    'Block' as type,
    count(*) as count
  from user_relationships
  join users on users.id = user_relationships.target_id and users.local
  where
    relationship_type = 1
  group by users.ap_id
),
aggcounts as (
  select
    account,
    jsonb_object_agg(sepcounts.type, sepcounts.count) data
  from sepcounts
  group by account
),
res as (
  select
    account,
    coalesce((data->'Block')::int, 0) as blocks,
    coalesce((data->'Flag')::int, 0) as reports
  from aggcounts
)
select
  res.*,
  (res.blocks + res.reports*10) as highscore,
  coalesce(users.avatar->'url'->0->>'href', '') as avatar,
  coalesce(users.name, '') as name,
  coalesce(users.nickname, '') as nickname
from res
join users on users.ap_id = res.account and users.is_discoverable
order by highscore desc
limit 100
`

type highscore struct {
	Account   string `db:"account" json:"account"`
	Blocks    int    `db:"blocks" json:"blocks"`
	Reports   int    `db:"reports" json:"reports"`
	Highscore int    `db:"highscore" json:"highscore"`
	Avatar    string `db:"avatar" json:"avatar"`
	Name      string `db:"name" json:"name"`
	Nickname  string `db:"nickname" json:"nickname"`
}

const highscoreResultsKey = "results"

func fetchResults(
	ctx context.Context,
	stmt *sqlx.Stmt,
	cache *gocache.Cache,
	instance string,
) (hs []*highscore, err error) {
	if v, ok := cache.Get(highscoreResultsKey); ok {
		if hs, ok = v.([]*highscore); ok {
			return
		}
	}

	err = stmt.SelectContext(ctx, &hs, instance)
	if err != nil {
		return
	}

	cache.SetDefault(highscoreResultsKey, hs)

	return
}

func main() {
	var listenAddr, postgresConn, instanceName string

	var cacheTTL time.Duration

	flag.StringVar(&listenAddr, "listen", ":8080", "listen address")
	flag.StringVar(&postgresConn, "postgres", "postgres://pleroma@127.0.0.1:5432/pleroma", "pleroma db")
	flag.StringVar(&instanceName, "instance", "example.com", "instance domain name")
	flag.DurationVar(&cacheTTL, "cache", time.Second, "cache TTL")

	flag.Parse()

	cache := gocache.New(cacheTTL, cacheTTL)

	db, err := sqlx.Connect("postgres", postgresConn)
	if err != nil {
		panic(err)
	}

	stmt, err := db.Preparex(queryHighscores)
	if err != nil {
		panic(err)
	}

	http.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		var (
			hs []*highscore
			bs []byte
		)

		hs, err = fetchResults(request.Context(), stmt, cache, instanceName)
		if err != nil {
			http.Error(writer, err.Error(), http.StatusInternalServerError)

			return
		}

		if hs == nil {
			hs = make([]*highscore, 0)
		}

		bs, err = json.Marshal(hs)
		if err != nil {
			http.Error(writer, err.Error(), http.StatusInternalServerError)

			return
		}

		writer.Header().Set("content-type", "application/json")
		writer.Header().Set("content-length", strconv.Itoa(len(bs)))
		writer.WriteHeader(http.StatusOK)

		_, _ = writer.Write(bs)
	})

	err = http.ListenAndServe(listenAddr, nil)
	if err != nil {
		panic(err)
	}
}
